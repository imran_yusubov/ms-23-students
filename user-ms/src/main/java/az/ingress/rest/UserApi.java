package az.ingress.rest;

import az.ingress.dto.UserRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserApi {

    @PostMapping
    public void createUser(@Validated @RequestBody
                           UserRequestDto userRequestDto) {
        log.info("Received create student request {}", userRequestDto);
    }


}
