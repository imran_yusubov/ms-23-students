package az.ingress.service;

import az.ingress.domain.StudentEntity;
import az.ingress.repo.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public StudentEntity createStudent(StudentEntity studentEntity) {
        return studentRepository.save(studentEntity);
    }

    @Override
    public List<StudentEntity> list() {
        return studentRepository.findAll();
    }

    @Override
    public StudentEntity get(long id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("No student with id " + id + " found in db"));
    }

    @Override
    public StudentEntity update(StudentEntity studentEntity1) {
        return studentRepository.save(studentEntity1);
    }

    @Override
    public void delete(long id) {
        studentRepository.deleteById(id);
    }
}
