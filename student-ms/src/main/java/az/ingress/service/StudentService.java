package az.ingress.service;

import az.ingress.domain.StudentEntity;

import java.util.List;

public interface StudentService {


    StudentEntity createStudent(StudentEntity studentEntity);

    List<StudentEntity> list();

    StudentEntity get(long l);

    StudentEntity update(StudentEntity studentEntity1);

    void delete(long l);
}
