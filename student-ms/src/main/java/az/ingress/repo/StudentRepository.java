package az.ingress.repo;

import az.ingress.domain.StudentEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Arrays;
import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity, Long>,
        JpaSpecificationExecutor<StudentEntity> {
    List<StudentEntity> findByFirstNameAndLastName(String firstName, String lastName);

    List<StudentEntity> findByFirstNameLikeAndLastName(String firstName, String lastName);

    @Query("SELECT s FROM StudentEntity s WHERE  firstName= :firstName AND s.lastName= :lastName ")
    List<StudentEntity> listStudentsJPQL(@Param("firstName") String firstName1, @Param("lastName") String lastName);


    @Query(nativeQuery = true, value = "SELECT s.* FROM students s WHERE first_name= :firstName AND s.last_name= :lastName ")
    List<StudentEntity> listStudentsSQL(@Param("firstName") String firstName, @Param("lastName") String lastName);


}
