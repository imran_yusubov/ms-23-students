package az.ingress;

import az.ingress.domain.StudentEntity;
import az.ingress.repo.StudentRepository;
import az.ingress.service.StudentService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class StudentMs implements CommandLineRunner {

    private final EntityManager entityManager;

    private final StudentService studentService;
    private final StudentRepository studentRepository;


    public static void main(String[] args) {

        SpringApplication.run(StudentMs.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //******* Create Student ****/
        StudentEntity studentEntity =
                StudentEntity.builder()
                        .firstName("Parvin")
                        .lastName("Etibarli")
                        .age(25)
                        .studentNumber("4764674647")
                        .build();

        studentService.createStudent(studentEntity);

        //******* List Students ****/
        System.out.println(studentService.list());

        //******* Get One ****/
        System.out.println(studentService.get(5L));

        //******* Update One ****/
        StudentEntity studentEntity1 =
                StudentEntity.builder()
                        .id(4l)
                        .firstName("Mehemmed")
                        .lastName("Faracli")
                        .age(25)
                        .studentNumber("723453287")
                        .build();
        System.out.println(studentService.update(studentEntity1));

        //******* Delete One ****/
        studentService.delete(6L);

        System.out.println("============QUERY METHODS================");
        studentRepository.findByFirstNameAndLastName("Farid", "Faracli")
                .stream()
                .forEach(System.out::println);

        System.out.println("============QUERY METHODS LIKE QUERY================");
        studentRepository.findByFirstNameLikeAndLastName("Far%", "Faracli")
                .stream()
                .forEach(System.out::println);

        System.out.println("============CRITERIA API================");
        String firstName = "Farid2";
        String lastName = "Faracli";
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<StudentEntity> criteriaQuery =
                criteriaBuilder.createQuery(StudentEntity.class);
        Root<StudentEntity> root = criteriaQuery.from(StudentEntity.class);
        List<Predicate> list = new ArrayList<>();
        if (firstName != null) {
            final Predicate firstNamePredicate = criteriaBuilder.equal(root.get("firstName"), firstName);
            list.add(firstNamePredicate);
        }
        if (lastName != null) {
            final Predicate lastNamePredicate = criteriaBuilder.equal(root.get("lastName"), lastName);
            list.add(lastNamePredicate);
        }

        criteriaQuery.select(root).where(list.toArray(new Predicate[0]));
        final TypedQuery<StudentEntity> query = entityManager.createQuery(criteriaQuery);
        query.getResultList()
                .stream()
                .forEach(System.out::println);


        System.out.println("============CRITERIA API2================");
        studentRepository.findAll(listByFirstAndLastname("Farid", "Faracli"))
                .stream()
                .forEach(System.out::println);


        System.out.println("============JPQL================");
        studentRepository.listStudentsJPQL("Farid", "Faracli")
                .forEach(System.out::println);

        System.out.println("============SQL================");
        studentRepository.listStudentsSQL(null, "Faracli")
                .forEach(System.out::println);

        System.out.println("============PAGE REQUEST================");
        studentRepository.findAll(PageRequest.of(1, 10))
                .forEach(System.out::println);
    }


    public static Specification<StudentEntity> listByFirstAndLastname(String firstName,
                                                                      String lastName) {
        List<Predicate> list = new ArrayList<>();
        return (root, query, builder) -> {
            if (firstName != null) {
                final Predicate firstNamePredicate = builder.equal(root.get(StudentEntity.Fields.firstName), firstName);
                list.add(firstNamePredicate);
            }
            if (lastName != null) {
                final Predicate lastNamePredicate = builder.equal(root.get(StudentEntity.Fields.lastName), lastName);
                list.add(lastNamePredicate);
            }
            return builder.and(list.toArray(new Predicate[0]));
        };

    }
}
